package com.BanqueProjet.MaBanque.DAO;


import com.BanqueProjet.MaBanque.Models.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

}