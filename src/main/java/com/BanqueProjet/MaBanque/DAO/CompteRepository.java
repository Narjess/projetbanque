package com.BanqueProjet.MaBanque.DAO;



import org.springframework.data.jpa.repository.JpaRepository;

import com.BanqueProjet.MaBanque.Models.Compte;

public interface CompteRepository extends JpaRepository<Compte, String>{

	Compte findOne(String codeCpte);

}